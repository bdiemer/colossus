{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Colossus tutorial: MCMC fitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Colossus includes a basic MCMC fitting module based on the [Goodman & Weare 2010](http://adsabs.harvard.edu/abs/2010CAMCS...5...65G) algorithm, contributed by Andrey Kravtsov. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import print_function \n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to define a likelihood function which we are trying to maximize. For a quick demonstration, let's use a double Gaussian with correlated parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def likelihood(x):\n",
    "\n",
    "    sig1 = 1.0\n",
    "    sig2 = 2.0\n",
    "    r = 0.95\n",
    "    r2 = r * r\n",
    "    res = np.exp(-0.5 * ((x[:, 0] / sig1)**2 + (x[:, 1] / sig2)**2 - 2.0 * r * x[:, 0] * x[:, 1] \\\n",
    "            / (sig1 * sig2)) / (1.0 - r2)) / (2 * np.pi * sig1 * sig2) / np.sqrt(1.0 - r2)\n",
    "\n",
    "    return res"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running the MCMC is easy now: we need to decide on an initial guess for the parameters and a number of \"walkers\" (chains that run in parallel). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "param_names = ['x1', 'x2']\n",
    "x_initial = np.array([1.0, 1.0])\n",
    "n_params = len(param_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could just use the `run()` function to complete all the following steps in one function call, but for the sake of demonstration, let's break it down into the main steps.\n",
    "\n",
    "First, the `runChain()` function does the actual MCMC sampling. It takes more optional arguments than shown in the code below. By default, the MCMC is stopped when the Gelman-Rubin criterion is below a certain number in all parameters. Running this code should take less than a minute on a modern laptop. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running MCMC with the following settings:\n",
      "Number of parameters:                      2\n",
      "Number of walkers:                       200\n",
      "Save conv. indicators every:             100\n",
      "Finish when Gelman-Rubin less than:   0.0100\n",
      "-------------------------------------------------------------------------------------\n",
      "Step    100, autocorr. time  28.7, GR = [  1.318  1.323]\n",
      "Step    200, autocorr. time  51.1, GR = [  1.131  1.138]\n",
      "Step    300, autocorr. time  51.7, GR = [  1.086  1.090]\n",
      "Step    400, autocorr. time  52.7, GR = [  1.063  1.068]\n",
      "Step    500, autocorr. time  50.8, GR = [  1.049  1.055]\n",
      "Step    600, autocorr. time  49.5, GR = [  1.040  1.046]\n",
      "Step    700, autocorr. time  48.6, GR = [  1.035  1.039]\n",
      "Step    800, autocorr. time  47.3, GR = [  1.033  1.037]\n",
      "Step    900, autocorr. time  45.9, GR = [  1.029  1.033]\n",
      "Step   1000, autocorr. time  44.7, GR = [  1.025  1.028]\n",
      "Step   1100, autocorr. time  42.0, GR = [  1.023  1.026]\n",
      "Step   1200, autocorr. time  41.7, GR = [  1.021  1.023]\n",
      "Step   1300, autocorr. time  41.1, GR = [  1.021  1.022]\n",
      "Step   1400, autocorr. time  40.6, GR = [  1.020  1.021]\n",
      "Step   1500, autocorr. time  40.5, GR = [  1.020  1.020]\n",
      "Step   1600, autocorr. time  41.5, GR = [  1.018  1.019]\n",
      "Step   1700, autocorr. time  40.5, GR = [  1.017  1.018]\n",
      "Step   1800, autocorr. time  41.3, GR = [  1.016  1.017]\n",
      "Step   1900, autocorr. time  40.4, GR = [  1.016  1.017]\n",
      "Step   2000, autocorr. time  39.5, GR = [  1.015  1.016]\n",
      "Step   2100, autocorr. time  39.3, GR = [  1.014  1.014]\n",
      "Step   2200, autocorr. time  38.9, GR = [  1.013  1.013]\n",
      "Step   2300, autocorr. time  38.1, GR = [  1.012  1.012]\n",
      "Step   2400, autocorr. time  38.0, GR = [  1.012  1.012]\n",
      "Step   2500, autocorr. time  38.1, GR = [  1.011  1.012]\n",
      "Step   2600, autocorr. time  37.3, GR = [  1.011  1.011]\n",
      "Step   2700, autocorr. time  36.7, GR = [  1.010  1.010]\n",
      "Step   2800, autocorr. time  35.6, GR = [  1.010  1.010]\n",
      "Step   2900, autocorr. time  35.7, GR = [  1.010  1.010]\n",
      "-------------------------------------------------------------------------------------\n",
      "Acceptance ratio:                          0.661\n",
      "Total number of samples:                  580000\n",
      "Samples in burn-in:                       140000\n",
      "Samples without burn-in (full chain):     440000\n",
      "Thinning factor (autocorr. time):             35\n",
      "Independent samples (thin chain):          12572\n"
     ]
    }
   ],
   "source": [
    "from colossus.utils import mcmc\n",
    "\n",
    "walkers = mcmc.initWalkers(x_initial, nwalkers = 200, random_seed = 156)\n",
    "chain_thin, chain_full, _ = mcmc.runChain(likelihood, walkers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the chain output, we can now compute the most likely values for the parameters as well as confidence intervals. We use the thinned chain for this purpose because the full chain's individual samples are highly correlated, leading to erroneous statistical inferences."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-------------------------------------------------------------------------------------\n",
      "Statistics for parameter 0, x1:\n",
      "Mean:              -5.000e-03\n",
      "Median:            -1.595e-02\n",
      "Std. dev.:         +9.745e-01\n",
      "68.3% interval:    -9.792e-01 .. +9.680e-01\n",
      "95.5% interval:    -1.925e+00 .. +1.986e+00\n",
      "99.7% interval:    -2.819e+00 .. +3.151e+00\n",
      "-------------------------------------------------------------------------------------\n",
      "Statistics for parameter 1, x2:\n",
      "Mean:              -1.308e-02\n",
      "Median:            -1.386e-02\n",
      "Std. dev.:         +1.952e+00\n",
      "68.3% interval:    -1.972e+00 .. +1.924e+00\n",
      "95.5% interval:    -3.925e+00 .. +3.972e+00\n",
      "99.7% interval:    -5.824e+00 .. +5.848e+00\n"
     ]
    }
   ],
   "source": [
    "mcmc.analyzeChain(chain_thin, param_names = param_names);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To elucidate the individual and joint likelihood distributions of the parameters, it is helpful to plot the chain output. The following function does just that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.colors import LogNorm\n",
    "import matplotlib.gridspec as gridspec\n",
    "\n",
    "def plotChain(chain, param_labels):\n",
    "\n",
    "    nsamples = len(chain)\n",
    "    nparams = len(chain[0])\n",
    "\n",
    "    # Prepare panels\n",
    "    margin_lb = 1.0\n",
    "    margin_rt = 0.5\n",
    "    panel_size = 2.5\n",
    "    size = nparams * panel_size + margin_lb + margin_rt\n",
    "    fig = plt.figure(figsize = (size, size))\n",
    "    gs = gridspec.GridSpec(nparams, nparams)\n",
    "    margin_lb_frac = margin_lb / size\n",
    "    margin_rt_frac = margin_rt / size\n",
    "    plt.subplots_adjust(left = margin_lb_frac, bottom = margin_lb_frac, right = 1.0 - margin_rt_frac,\n",
    "                    top = 1.0 - margin_rt_frac, hspace = margin_rt_frac, wspace = margin_rt_frac)\n",
    "    panels = [[None for dummy in range(nparams)] for dummy in range(nparams)] \n",
    "    for i in range(nparams):\n",
    "        for j in range(nparams):\n",
    "            if i >= j:\n",
    "                pan = fig.add_subplot(gs[i, j])\n",
    "                panels[i][j] = pan\n",
    "                if i < nparams - 1:\n",
    "                    pan.set_xticklabels([])\n",
    "                else:\n",
    "                    plt.xlabel(param_labels[j])\n",
    "                if j > 0:\n",
    "                    pan.set_yticklabels([])\n",
    "                else:\n",
    "                    plt.ylabel(param_labels[i])\n",
    "            else:\n",
    "                panels[i][j] = None\n",
    "\n",
    "    # Plot 1D histograms\n",
    "    nbins = min(50, nsamples / 20.0)\n",
    "    minmax = np.zeros((nparams, 2), float)\n",
    "    for i in range(nparams):\n",
    "        ci = chain[:, i]\n",
    "        plt.sca(panels[i][i])\n",
    "        _, bins, _ = plt.hist(ci, bins = nbins)\n",
    "        minmax[i, 0] = bins[0]\n",
    "        minmax[i, 1] = bins[-1]\n",
    "        diff = minmax[i, 1] - minmax[i, 0]\n",
    "        minmax[i, 0] -= 0.03 * diff\n",
    "        minmax[i, 1] += 0.03 * diff\n",
    "        plt.xlim(minmax[i, 0], minmax[i, 1])\n",
    "\n",
    "    # Plot 2D histograms\n",
    "    for i in range(nparams):\n",
    "        ci = chain[:, i]\n",
    "        for j in range(nparams):\n",
    "            cj = chain[:, j]\n",
    "            if i > j:\n",
    "                plt.sca(panels[i][j])\n",
    "                plt.hist2d(cj, ci, bins = 100, norm = LogNorm(), density = True)\n",
    "                plt.ylim(minmax[i, 0], minmax[i, 1])\n",
    "                plt.xlim(minmax[j, 0], minmax[j, 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function is not part of the main body of Colossus because it relies on matplotlib. Here is its output for the chain above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAmQAAAJGCAYAAAD1dZmBAAAAOnRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjEwLjAsIGh0dHBzOi8vbWF0cGxvdGxpYi5vcmcvlHJYcgAAAAlwSFlzAAAPYQAAD2EBqD+naQAARmhJREFUeJzt3XtsXOd57/vf8Da8iDND3SxLIjV2kp29XUcimyLZaRuRPE1gBGhkEUVPUTSWRCNoURSVRMU9gQJUNOUiRE7SyjZOUex9ithyWgQbB6ko9Q/DO0ZE0UazU+yEtGIkbdJYI9KSZcsUh4vX4WXm/PGuywxJiXe+HM73AwjDWeudmTUczuiZ533W84YymUxGAAAAsKbI9gEAAAAUOgIyAAAAywjIAAAALCMgAwAAsIyADAAAwDICMgAAAMsIyAAAACwjIAMAALCsxPYBYL50Oq3bt2+rurpaoVDI9uEAWKZMJqORkRHt3btXRUV87wWwOAKyTej27duqra21fRgAVmlgYED79++3fRgA8gAB2SZUXV0tyXyYRyIRy0ezvhzHUW1tbUE817l47lv3uXvPz3svA8BiCMg2IW+aMhKJbMn/rBZSSM91Lp771n3ulBwAWCqKGwAAACwjIAMAALCMgAxWhcNhtbe3KxwO2z6UDcdzL8znDgALCWUymYztg0Aux3EUjUY1PDy8petrgK2K9zCA5SJDBgAAYBkBGQAAgGUEZAAAAJYRkAEAAFhGY1jAklDoWznXM5lnLB0JAMA2MmQAAACWEZABAABYRkAGAABgGQEZAACAZRT1AxtgbgE/AADZyJABAABYRkAGAABgGQEZAACAZQRkAAAAlhGQAQAAWEZABgAAYBltL4BNYqHWGKxvCQCFgQwZAACAZQRkAAAAlhGQAQAAWEZABgAAYBlF/cA6YO1KAMBy5FVAdv36dXV1dUmSbty4odbWVh0+fDhnjOM4am9vV01NjYaGhtTa2qqDBw+uy5jljAMAALifvAnIenp61NfXp3PnzkkygdChQ4fU0dGhY8eO5Wy7fPmyDh48KMdx1NjYqBdeeMEP3NZqzHLGAQAAPEje1JC1t7fr6tWr/vVIJKKmpiadOnXK33bq1CkdPXrUz1BFIhF1dHTo+PHjaz5mOeMAAAAeJG8Csra2NrW2tuZsSyaTOde7urrU3Nycs62pqUmJRELXr19f0zHLGQcAAPAgeTNleeTIkZzrjuOou7tbbW1tkqT+/n4lk0nFYrGccZFIRJLU19enWCy2JmMOHjy4pMejlgwAACxF3mTIsvX39+vUqVM6ceKEX1OWSCQeeJtEIrFmY5b6eAAAAEuRNxkyz/nz5yVJsVhMLS0tlo9mfTmOk3M9HA4rHA5bOhoA95NKpZRKpfzrc9+7ALCYvAvIvIyYJDU3Nysej+ull16yeETrp7a2Nud6e3u7nn32WTsHA+C+Ojs71dHRYfswAOSxvAvIsrW2tur48ePziv23ioGBAb8mTRLZMWCTOnv2rM6cOeNfdxxn3hcqAHiQvKghcxxHLS0t885cjMfjkqTu7u5F+341NTWt2RhJSx63GpFIJOcfARmwOYXD4XnvVwBYjrwIyPr6+tTV1XXfQnkvMKuvr1d3d3fOvp6eHn/fWo5ZzjgAAIAHyYuA7PDhw+ro6JjX+uKll15SfX29jh49Kknq6OjIaR4rmezZ6dOn/W+sazVmOeOAlQqFvpXzDwCwNeVNDdnp06d1/vx5xeNxxeNxdXd3K5FI6Nq1a37wc+TIEfX19amlpUVtbW3q7u5Wb2+vLl686N/PWo1ZzjgAAIAHCWUymYztg1iO/v5+JRIJxeNx1dXVLTjGcRz19fVtyJjljFsqx3EUjUY1PDxMpi1PrVc2K5N5Zl3uF2uL9zCA5cq7gKwQ8GGe/wjIChvvYQDLlRc1ZAAAAFsZARkAAIBlBGQAAACWEZABAABYRkAGAABgGQEZAACAZQRkAAAAluVNp35gs2JJIwDAapEhAwAAsIyADAAAwDICMgAAAMsIyAAAACwjIAMAALCMgAwAAMAyAjIAAADLCMgAAAAsozEskEcWakKbyTxj4UgAAGuJDBkAAIBlBGQAAACWEZABAABYRkAGAABgGQEZAACAZQRkAAAAlhGQAQAAWEZABgAAYBkBGQAAgGUEZAAAAJYRkAEAAFhGQAYAAGAZi4sDy7TQAt8AAKwGGTIAAADLCMgAAAAsIyADAACwjIAMAADAMgIyAAAAywjIAAAALCMgAwAAsIyADAAAwDICMgAAAMvo1A/kubkrB2Qyz1g6EgDASpEhAwAAsIyADAAAwDICMgAAAMsIyAAAACwjIAMAALCMgAwAAMAyAjIAAADLCMgAAAAsIyADAACwjIAMAADAMgIyAAAAywjIAAAALGNxceAB5i7cDQDAesibgKy/v19dXV1KJpO6ceOG2tradPDgwZwxjuOovb1dNTU1GhoaUmtr67qNWc44AACAB8mLgOz69etKJBI6efKkf72xsVEvvPCCjh07JskER4cOHdLly5d18OBBOY7jjzl8+PCajlnOOAAAgMXkRQ1ZV1eXjhw54l8/ePCgTpw4oVOnTvnbTp06paNHj/oZqkgkoo6ODh0/fnzNxyxnHAAAwGLyIiBrb29XS0tLzraGhgYlk0n19PRIMkFbc3NzzpimpiYlEgldv359TccsZxwAAMBi8iIgO3369LyALJlMSpJisZj6+/uVTCYVi8VyxkQiEUlSX1/fmo2RtORxAAAAS5EXNWQXLlyYt623t1fxeFwHDx70s2T3k0gklEgk1mRM9uVi4wAAAJYiLwKyufr7+/Xyyy/r2rVrtg9lXTmOk3M9HA4rHA5bOhoA95NKpZRKpfzrc9+7ALCYvJiynOv48eO6ePHilj+bsba2VtFo1P/X2dlp+5AALKCzszPnvVpbW2v7kADkmbzLkLW1tam1tdVvd7GVDQwM+HVpksiOAZvU2bNndebMGf+64zgEZQCWJa8CsldeeUUNDQ05vcckLZopa2pqWrMxS328tRCJRHICMgCbE+UEAFYrb6YsvVYS2Zkxr3O/JNXX16u7uzvnNl6xf319/ZqOWc44YKOFQt+a9w8AsLnlRUDW39+vl156SfF4XD09Pf6/S5cuqa6uTpLU0dGhq1ev5tyuu7tbp0+f9rNMazVmOeMAAAAWkxdTlk8++aT6+vr0/PPP52zPzkQdOXJEfX19amlpUVtbm7q7u9Xb26uLFy+u+ZjljAMAAFhMKJPJZGwfxFpyHEd9fX2Kx+N+9my9xixn3HKfQzQa1fDwMNk2y7bKdF8m84ztQygovIcBLNeWC8i2Aj7MNw8CMqwE72EAy7VuNWS/8zu/s153DQAAsKWsW0A29wxEAAAALGzJRf1/9Vd/tZ7HAQAAULCWHJBlMhm1t7cv+Y5DodCKDggAAKDQLHnK8tSpU4rFYkqn00v6BwAAgKVZckAWiURUX1+vN998c0njOXkTAABgaZZV1N/a2qp4PL6ksdeuXVvJ8QAAABScZXXqf+qpp5Y89rOf/eyyDwYAAKAQrVvbi7fffnu97hoAAGBLWfFals8995weffRR/dEf/dG8fW+88YaOHj2qwcHBVR0csNG2Smd+AEB+WXGG7NChQ3rqqaf0zDO5S7J85zvfUWNjo44fP77qgwMAACgEK86QHTlyRN3d3Tp69Kj6+vrU1dWlkydPqqurS5cvX9YXv/jFtTxOAACALWvFAZkkHT58WDdv3lRjY6MOHDigeDyut956S7W1tWt1fAAAAFveqov6L1++rEQioUOHDqm3t1dXrlxZi+MCAAAoGKsKyJ5++mkdP35cL774on7wgx+or69Pf/mXf6nPfe5zGhsbW6tjBAAA2NJWHJA999xz6urqUnd3t770pS9Jkg4ePKhEIqHBwUEdOHBgzQ4SAABgK1txDVl9ff2C9WKRSES9vb16+umnV31wANbGQu08MplnFhgJALBhxQHZYmdRfvvb317pXQMAABSUdevUDwAAgKUhIAMAALCMgAwAAMAyAjIAAADLCMgAAAAsIyADAACwjIAMAADAMgIyAAAAywjIAAAALCMgAwAAsIyADAAAwLIVr2UJ5LuFFtwGAMAGMmQAAACWEZABAABYRkAGAABgGQEZAACAZQRkAAAAlhGQAQAAWEZABgAAYBkBGQAAgGU0hgUK1NzGuJnMM5aOBABAhgwAAMAyAjIAAADLCMgAAAAsIyADAACwjIAMAADAMgIyAAAAywjIAAAALCMgAwAAsIyADAAAwDICMgAAAMtYOgkFYe4yQQAAbCZkyAAAACwjIAMAALCMgAwAAMCyvKshu379uvr6+nTs2LF5+xzHUXt7u2pqajQ0NKTW1lYdPHhwXcYsZxwAAMCD5EVA1t/fr5dfflmS9NJLL6m1tXXeGMdxdOjQIV2+fFkHDx6U4zhqbGzUCy+8oMOHD6/pmOWMAwAAWExeTFnW1dXp3LlzOnfunOLx+IJjTp06paNHj/oZqkgkoo6ODh0/fnzNxyxnHAAAwGLyIiBbiq6uLjU3N+dsa2pqUiKR0PXr19d0zHLGAQAALGZLBGT9/f1KJpOKxWI52yORiCSpr69vzcYs9fEAAACWKi9qyBaTSCQW3b9WY5b6eEC+Wah5bibzjIUjAYDCsyUCsq3KcZyc6+FwWOFw2NLRALifVCqlVCrlX5/73gWAxWyJKcutqra2VtFo1P/X2dlp+5AALKCzszPnvVpbW2v7kADkGTJkm9jAwIBflyaJ7BiwSZ09e1ZnzpzxrzuOQ1AGYFm2REC2WN+vpqamNRuz1MdbC5FIJCcgA7A5UU4AYLW2zJRlfX29uru7c7b19PT4+9ZyzHLGAQAALGbLBGQdHR26evVqzrbu7m6dPn3azzKt1ZjljAMAAFhM3kxZnj9/XpLp8ZVMJiVJ8XjcX9PyyJEj6uvrU0tLi9ra2tTd3a3e3l5dvHjRv4+1GrOccQAAAIsJZTKZjO2DWEuO46ivr0/xeFx1dXXrOmY545b7HKLRqIaHh8m2rZGFemxhcfQhWxnewwCWa8sFZFsBH+Zrj4BsZQjIVob3MIDl2jI1ZAAAAPkqb2rIgOUgIwYAyCdkyAAAACwjIAMAALCMgAwAAMAyAjIAAADLCMgAAAAsIyADAACwjLYXAO5rbvsQGsUCwPogQwYAAGAZARkAAIBlBGQAAACWEZABAABYRkAGAABgGQEZAACAZQRkAAAAltGHDHlvbq8sAADyDRkyAAAAywjIAAAALCMgAwAAsIyADAAAwDICMgAAAMsIyAAAACyj7QWAJVuoxUgm84yFIwGArYUMGQAAgGUEZAAAAJYRkAEAAFhGQAYAAGAZARkAAIBlnGWJvMNi4gCArYYMGQAAgGUEZAAAAJYRkAEAAFhGDRmAVZlb00fnfgBYPjJkAAAAlhGQAQAAWEZABgAAYBkBGQAAgGUU9WNTowksAKAQkCEDAACwjIAMAADAMgIyAAAAy6ghA7CmFqr7o1ksADwYGTIAAADLCMgAAAAsIyADAACwjBoybCr0HQMAFCIyZAAAAJaRIQOw7uZmPjnrEgBykSEDAACwjIAMAADAMqYsYQ0F/AAAGARkADYc3fwBIBdTlgAAAJYRkAEAAFjGlCU2DDVjAAAsjIAM62LpwdeMpB9I+j9UeH+OPPfs506vMgCFjClLWDYj6fvuZaHhuRfmcweA+QrtaznWCdORWGuciQmgkBCQYdkIvmALQRqArYqAbBPKZDKSpFu3bslxHH97OBxWOBxe8DbR6Is514eHTy46ZnOYnHNZSHjua/HcQ6G/WvV9rJT3PkulUkqlUv72kZERScF7GQAWE8rwibHpvPvuu6qtrbV9GABWaWBgQPv377d9GADyAAHZJpROp5VIJFRaWqpQKORvf1CGDIA9czNkmUxG09PTisfjKiri3CkAiyMgAwAAsIyvbgAAAJYRkAEAAFhGQAYAAGAZARkAAIBlBGQAAACWEZABAABYRkAGAABgGQEZAACAZQRkAAAAlhGQAQAAWEZABgAAYBkBGQAAgGUEZAAAAJYRkAEAAFhWYvsAMF86ndbt27dVXV2tUChk+3AALFMmk9HIyIj27t2roqLVfe/l8wDIX8v5LCAg24Ru376t2tpa24cBYJUGBga0f//+Vd0HnwdA/lvKZwEB2SZUXV0tybyAkUjE8tGsL8dxVFtbWxDPdS6e+9Z97t7z897Lq7EVPw+2+uu/VPwetv7vYDmfBQRkm5A3LRGJRLbkH+hCCum5zsVz37rPfS2mGLfy58FWfE4rwe9h6/8OlvJZQFE/AACAZQRkAAAAlhGQwapwOKz29naFw2Hbh7LheO6F+dzB6+/h98DvIFsok8lkbB8EcjmOo2g0quHh4S09pw5sVWv5HubzAMhfy3n/kiEDgGX6fNHv6/NFv2/7MABsIQRkAAAAltH2AgCW6fvp/8/2IQDYYsiQAQAAWEZABgAAYBlTlgAAbKBQ6Fs51zOZZywdCTYTAjIAACyaG6BJBGmFiClLAAAAywjIAAAALCMgA1Awnvhku574ZPsDx9D0FYANBGQAAACWUdQPoGCke3+2qtt72bXXftyxFocDAD4yZAAAAJaRIQOwJX2u8ev+z69f+5qkpS159KAxZMawXAu1tAAWQoYMAADAMgIyAAAAy5iyBLDp/fbvmWmfN7+39O7ls+XF/s+f+cO/liT98LtfWfR2XsuLpUxvAsBaIUMGAABgGRkyAJvCgzJTy8mMHT7yTfNDOPi+GfvRezljFmpf8YVHzWOkfvdTkqRf/9ML/r6f/F3bkh8fAFaCDBkAAIBlZMgAbApzM2PZSxzNbTeRvc9r9jr6B/9VkpTaZz7Wou9M+WNmEjclSc1PfEOSVLwtLElq+LMgC9b7zuZuT+A4Ts71cDiscDhs6WgALCSVSimVSvnX575vH4QMGQDkgdraWkWjUf9fZ2en7UMCMEdnZ2fO+7S2tnbJtyVDBgB5YGBgQJFIxL9Odmxrm9tQNpNZeh0l7Dl79qzOnDnjX3ccZ8lBGQEZAKu8Yv65Firu96YctbMy6w5+Q1LQ0sIrzk/XVPlDptwxV1/76qLHsVnbXUQikZyADMDms5pSAqYsAQAALCNDBsCquRkprwlsNi/rVe5eT376YX9f8VRGUlDoP1u7XZI0FS31x/Rc+Yuc+/G8mlXIv1kzYwAKAxkyAAAAy8iQAdgUPtf4dUnSm9e+JimrXkxSsZv1et3dl51Fq+69I0l6/4l9kqTtP5uQFGTFpKA+bPjYZyRJNT8dXvsnAACrQIYMAADAMjJkADaMV8M19vhD/ray4WlJUuiNXkkLL33kHCiTFGTRRg5V+PsmdpjM2I7/9i85j/WJrwRNXx/6bIMkKfrKDyVJr1EvBmCTIUMGAABgGRkyAADWwNxmrsByEJAB2DD3fmuvJCn2s6CoftZdV9L5I1NwPxUJSZLSQdcKVQ/MmjGPmMYX2/89WKdycrv5GJto+bQkabrSJP5rfjnjjykeNWvLJd3HAIDNhilLAAAAy8iQAVg3cxuxVn5slyTp1u/E/G1eJuzAd29Jkm4fMUX6JePB7cZ3F0uSQiZRprsHy/x91e+mJUlje8yYPa+a+8leOum1H3es5mkAwLojQwYAAGAZGTIAa8JrvloSP+Bv85Y4Gvq4+e5XcddsDyeD26Wi5tLLjE1vc3ekgzHT1eYy+o7ZOLk9+C5Zfs/UijlxkzXzMmNkxQDkEzJkAAAAlpEhA7AqXmYs4zZfff+xinljtg2YBcCL3ZMjhz8a8vfNeMNDZkzkHbPPiQe3r+43l/ceM98hi6bnH8f+/5mUFGTGvMXGJSnd+zNJLCAOYPMiQwYAAGAZARkAAIBlTFkCWLbsdhbeVOXr174mSfr1Pw3WkJxyi/HTpWYaMlVjpiUr3wvuayqaybnvye3msnQs2JaqMZflg+bSaxQrSVdf+2rOMXnrXb5OUT+2kIVWAchknllgJPIVGbJVOn/+vPr7TYGL4zi6cuWKenp6LB8VAADIJ6FMJpNZfBjuJxQK5Vw/ffq0Lly4cJ/RS+M4jqLRqIaHhxWJRFZ1X8B68zJSY3vD8/Z57S5my3O3p8uCj53QjHkPeU1fvctMVv6+dMRc7vypaXHRc+Uv/H1e8f5mKtxfy/cwnwf5Y6PXsiRDtvkt5/3LlOUqnThxQs3NzZKkpqYm1dXVWT4iAACQbwjIVumRRx7RsWPHbB8GsCG8bFTRUFDgNdmwR5I0GzaZrnu/lnUDt7nrzDb3hxKTGSuayKqWKDbb0uW5+zKlQRat8o65rLg1KinIyklSsXu5GTJjALBS1JABAABYRoZslYaGhnT+/HlJ0o0bN9TQ0KCTJ09aPioAAJBPCMhWKZFI5BTxt7S0SNKaBGWO4+RcD4fDCofnF04D6+2Tf2z+xkP1ZuHJdFnM3zf8ETO1mCk119OlwSKUmXLzc1GZqdTPOGbQbGTGH1MyZD6G0u7cY4m7q/z94ISZ8qS5vdeF31sdQAqWvPS22Zi6TKVSSqVS/vW5710AWAxTlqt06dKlnOutra1qb2+/z+jlqa2tVTQa9f91dnauyf0CWFudnZ0579Xa2lrbh4QNEAp9K+cfsBpkyNZYLBZTMplUT0+PDh8+vKr7GhgYyDlNluwY1pOXYSqJH/C3JT/9sCSpzL3+wW+YrFWmOCi4z7iF+hk36xUqCjJkpWGT2ZoaMfcQqjD7vFYXkjQTNWOKx93vh+6usuHgMUpHzRi/6WtWFsxmZsxz9uxZnTlzxr/uOA5BGYBlIUO2Ci0tLbpy5cq63X8kEsn5R0AGbE7hcHje+xUAloMM2Sr09fXN25ZIJBSLxVRfX7/hxwOshrcE0nTWtuFHzXe28Tq3W2vabU0xGXyXy4TdOrFSMyZcHtxDOmPSXeWxSUlSaszNtWU1VA6Nm+Kx4jFzn6Vu+VXl3WB5pHsfN7fb/f/8i6TcGjLaXQDYCsiQrUJbW5uamppytl26dEltbW18QwYAAEtGhmwVTpw4oeeff17xeFzxeFwXLlxQQ0ODzp07Z/vQgCXzFuWeevwhSdKHB4OPhVTMrePyyrkq3LMlI0EWrLjEZMi2R0yz2KnZ4uD2U+asSi9T5su6Wuw2gi0bNtenzImcGn40OI7tP5/KuTlZMQBbDQHZKkQiEZ07d079/f1KJBK6ePEimTEAALBsBGRroK6ujjUsAQDAihGQAQXKW5dysHmvJGkkbuYRp2qyGrt67S3cwv2ybWbqMFwWNHYNhcwYb6oyq15f4TIztZlOm2nJ6SLzkZMZCT56Qu7DFbmzoB/9f29JkiY/tssfU/bhuCTpNaYqAWxRFPUDAABYRoYM2GK8In1JevWd3O7hn37qb/yfx5tikqSZKnN9epvb4DUaFOyHq0xGrMjNglWEzb7iUJBFC5eabNmsmwVLzWSdFOD+PDPrfve757a9KM9qHjtiUmrhpHkMLzPmZcUkqWho7H5PFwC2BDJkAAAAlpEhA7aY7KyYVyfmZZjGW/b5+6ZiubfL7DaLY4crggyZlxGrKDWX5SXmcnv5hD9m0s2CDU6YVFt29mzWzYylBivMBvcr4LZfBh89Ve+ZzNismzwr+f7/Nj80PDbvOXnZv7mZPwDId2TIAAAALCNDBmwR3sLboTd6/W1eA9WGP7sgSRrbHyzYnY6Z2q/iSpP1CrtLH+2JOv6Y0mKzzWvsWulmyEqysmBefVlNhan5en+0OtjnLTQ+bW5fPOGeghkchjJuH9nqd6dzjnkhZMYAbFVkyAAAACwjIAMAALCMKUsgj3y+6Pf9n+dO7U3udKviWz7tbzt00kxVjnzMzBGmtwcF+5URU5hfWmKmJXdvG5Uk7SwPWkyUFeVOWabdRSgnZ0v9MaPT5nGnZs3HyUQq2DfxYaX5wb198ZS5nK0IjrvqPXca1CvmX8JzBSCFQrlT+JnMM/cZiXxAhgwAAMAyMmRAHvCyRQtlin7798y35Nkyk32argzWLhqtczNjVSbTFd6W8veVuUX8XjH+3kpTzD86E/bHeMX7XuF+kXIvJSnjZr+mZkx1/oRTHhxc2uzLVJjHKv+V+Q5YcTe4/eR28zH0wznPjawYgEJChgwAAMAyMmRAHvCyRc1PfMPf5mWWUrtNZmrkgNk+Wxm0pEhXmJ/31N2TJJWVBIuCV5eabNmj2wZzHmt7WVBDlkqbx6gsNnVeA+M1kqTxmVLN5S+ZNBp8rLglaArfMuNLTZmaYj8bDsawLBIAkCEDAACwjQwZAACLmHtGI7DWCMiATWhuEb93vTx+wB+T/IJZl9J5xBTIF6fc1hSxYFrSa22xq9LMFW4Pj/v7St35xEjJRM712UyQOC8NzeZceu0vPhjb5o8ZnTQnARQVmePIauKvsiFzX8Xu0pdlI2bnWDzo5v/mjzsW/iUAQAFhyhIAAMAyMmTAJua1tBj709+ct298j7nMlJnM1PQeU6S/Y8eoP2Z/tSmeLy82WbOHwsE6lduKzfhK9zKVNoX32Rkyz/tTkZzrJcVZa1m6mbGxIdPttWw4uL2XGat+14yvum0eq3Tgnj/m8JFvSpJ6rvzFvMcFgEJBhgwAAMAyMmTAJpT5bIMk6c3vmaVQHv+/zBJI00HplaYjJjOV2T4lSYrETH3YzsqgjcSucpMtezhsMmXRkqCGrNhr8uoWfZUWz+Zcl6SJtFkWaWzGXDrTpl7MmQiav44OmuWRikdN+w23JM087g/NldevfS3n+WUvi9TzDsXSAECGDAAAwDIyZMAmNBU19Vyf+IrJjHlfnaZiQfYqvM9kwiKVk5KkfW692K5wUEPmNXndXmIuo8VBhmwyYx6jLGTqy8bSJvs1PhssnZScNnVho25m7K57duX4aDCmdNB8jJQlzRmY0XeCYxz6uLn95xq/LinIlLEsEgDkIkMGAABgGQEZAACAZUxZApZ4LS28wv3P/OFf+/uGD5q35nSVuT5bbgrwK+tG/DGRCjNVubPCTEd+ZNuHkqSakqCo3/u5PGTWoqwqSvn7ymW2JWdNUb7X9qI4q6h/Om0K9T+cMAcyOuE2gb0TTFkWTZmpym23zDFu+x//y9/H1CQALA0ZMgAAAMvIkAGWVFz6kaSgMergp4K3Y5mpz1cmarJOpQdMof7+WNIfs9ttaRErNa0l9pUNSZIqs7Jgu0pMI1hv6SOvkF+SRmaD1hWSdCsVkySNZRX13xypkSQNj5uxMymTMau+GfLHeK043IcgKwYAK0CGDAAAwDIyZMAG+8Kjz+RcH3zMvA2LpoNtI//ZXNm207Sp2BMxma6d5UF9WF2FWX5oX9hkxvaUJCVJ5Vl3NHcZpDIvjSWpyG0MO+pmyiZmTfPXm6M1/pjxabNtPGnaVxTfMxm2ouButP3f3Iay05l5z+9Vmr4CGyYUmv9+y2SeWWAkNiMyZAAAAJaRIQM2QPZSQc4ffUaSNPp/7pMU1GBN7QjSTpXbTV1YvMZkwQ5sM5e7y4KzLHeXmqzZ3MxYVWjKHzMbMrVeXtPXyXRQQzbuLov04bRp9vrehFlAfCQV1JZ98H7U/DBmPipC00HtmMerhfNqx7Kfq5ctI1MGAA9GhgwAAMAyAjIAAADLmLIENoA3TSlJYw+bab8pdzZweodZS7J6T7AGZV3MFOrvqzT9L+Llg5Kkh0uH/DG73ZYW5e5alJVFZqpyOquQf9ZtczGdMe0qvCawknRvxkxVvp9ypyqn3PUqh6qDA58wt4v+0txn5fumaWzpWNA8dm6bC9perA/HcXKuh8NhhcPh+4wGYEMqlVIqFbQemvu+fRAyZACQB2praxWNRv1/nZ2dtg8JwBydnZ0579Pa2tol35YMGbCOvAL36s82+NuG/otpITEdNUX8u2pN1mtnVdDSorbKbHu0wiyH5GXG9mVlyLzifS9DNiWTzcpudeFlxu7NmmzYeDrIqNyY2CFJenfMpOrujZvsWfGvKvwx0XfNZThpMmI//O5Xcp4XNs7AwIAikYh/newYsPmcPXtWZ86c8a87jrPkoIyADADyQCQSyQnIAGw+qyklICADVsnLFhU1POZvKxoy2a7BP/lNSZLzaMbfN1NtMmNlO01ri91Vpnbs0W2D/pgDbmZsb2lSkrSvxGTGqosm5z1+qbsY+JSbDZt2M2WSlJw1i4LfnTF1Yb8a3+3vS4xslyRNuM1fnX6TKSsPumZobK+5LJkwdW/NT3xDknSVOjEAWFMEZAAAzLFQ13tgPVHUDwAAYBkZMmCVsqcqPZMf2yVJGv6YmaqcrQraRFQ/bLrtP7rdTFHWVXoF/Hf9MY+GP5AkxYrMWpbeVGWVW8AvSUVu0/xJt4jfa3cxmQm68XtTlXdSZjpyZDrowu+4HfkH3zd1Sd76lEXBQ6jyXXP8kRvm8UNv9M57rgCA1SNDBgAAYBkZMmCVhj5hsk+hYClKDT5u0lezu0yDwKpIUIzvNX3dVW6K+R+vMr0ldpUEDQT3FJuGsF5Li6ibtirOWkpyPG2upDPmMpk2bStuT9f4Y96bipl902bfrbHgLL2kY7YVDZuMWtk9cz+lQfcNn5cZo+krAKwPMmQAAACWkSEDliC7EerM539DknTv46ZdxMye3KWQJGlmr8mM7dhp6sX2VI34+z5abWrFvJqxvW6zV69eTJKq3WWQYkVuDZq7vVhBimzSWw4pbRq5jsyaS69uTJJuu7Vjv0iamrZ7o8HSSbP3TK+cikFzn5Eb5rGKZoIWHZFfmKzda2TGAGBdkSEDAACwjAwZ8AALNX0t+f7/liRlfs00fXXLtDS1e9ofU1VtasbqIklJ0ke2fejve7TcnEFZW2rOstzj1o7tKAoWpK0MufVcodzvTLez6tTG3LMpvdqx/0g9JEm6MbHTH/Pze3skSR/cM1mzdCpoGlvxnvnZq3370XfO5DxnicwYAGyUTZche/PNN/Xmm2/ed//IyMgD9wMAAOSbTROQ/cM//IN27NihxsZGNTY2aseOHfrbv/3beeP6+vrU2Nho4QgBAADWx6aYsvznf/5nHTt2TE1NTWppaVEsFtMPfvAD/fmf/7l6e3v193//97YPEQXKa/PwyT++4G+b+ayZqpwwdfKaPWDWpKyJTPhjDkRNoX5d5T1JwTSlFExV7i42hf6RkCng314UvB2L3OL9tEyB/fuzZl5xJB00dr01Y9pbeG0ubqVikqT/GA6mLJ0JU7ifnjT3XfWLoGlsmdtlo/xe0LQ2+zkDADbOpgjITp06pZaWFn3ve9/ztz311FNqa2vTiRMn9OUvf5mgDACAZZq7Jmcm84ylI8FiNkVAlkgk9Morr8zbfvDgQXV3d6upqYmgDFZ4Be7Tp3/T3za5w1ymS032ats2U8C/ozLoqOo1fX2s8rYkaU/psL9vR7HZt6vY3G5nkclaVRaV+WNSGXOCwIez5tIr4M/OkN2dMU1efz72sCTpxqg5sORkhT9mYsxkyMLvus1fg96zfmbsh9/9yn2fPwBgY2yKGrJYLKbh4eEF90UiEXV3d+snP/mJvvzlL2/wkQEAAKy/TZEhO336tM6dO6empiZVVVXN2x+JRHTt2jUdP35cp0+f3vgDRMH4wqMmnT+TuClJGvwTkxkb2xc0S52JmWWMwjHTpuKAuxTSwxVB+um/VL0nKciMeUshSdJDbmbsoWKT7QqHTPbKy4pJ0kh62r00b9GBme2SpDvTQffZdyZ3m31jpobszohpbTE6GmTIwr80j/HIK7ckSa++E0xfPPHJ9pzn7mUDqSEDgI23KTJk586d05NPPqlPfOIT+spXFp4+qa6u1j/90z/p4MGDymQyC44BAADIR6HMJopuRkbMWWfV1dWLjNzaHMdRNBrV8PCwIpHI4jfAqnhZMUlKftrUY8V+ZDJciS/tkySNHwiyV2URc1bkw9tN1uvxGjP2YxXBmZTeckh+89fi4AxM72zKSJHJZE1nTMbNyQSNYW/PmO9Kd2bNeyExZU7pvJHa5Y/59xHTCPY/Bs1ZlSNJc3+h4eBMyuJJd1mkd8zlT/6u7X6/BqyhtXwP83lgx9xi+K2Cov6NtZz376aYsvQsNRB7++239fjjj6/z0djnOE7O9XA4rHA4bOloANxPKpVSKhUE1HPfuwCwmE0xZTnXc889p3/8x39ccN8bb7xRMI1ha2trFY1G/X+dnZ22DwnAAjo7O3Peq7W1tbYPCUCe2VQZMs+hQ4d09OhR9fb26lvfCtLG3/nOdwqqsH9gYCAnxUl2bH2MPf6Q//PIfvMd5f3PmKnLdNRMT1ZUB9mPmm3jkqT/HDVTlPvCSUnSzpIgK+JNVXqtLbKbvs6dqhxKmzHvzwbrTPa7TV+91hbvTcckSb8c3e2P+bcPzM8TI6ZwPzNljj36y+B7VsZ92B3/7V/MD0xZrouzZ8/qzJkz/nXHcQjK8shWnZ5EftmUAdmRI0fU3d2to0ePqq+vT11dXTp58qS6urp0+fJlffGLX7R9iBsiEolQMwLkAcoJAKzWpgzIJOnw4cO6efOmGhsbdeDAAcXjcb311lt868SKZLd4eO3HHZKk3/498614fF/WkkVmhSK/6Wuo2DRP3R0d8cd8JPKhJOmRyruSpANlJhu2r2TIH7OjyGS9dhWZrFdFKCi0v19mzMuKScFySF5m7K3h/ZKkO2NBneXUlDnukjumoWy16dShnX/3L/4Yv4XF35jMmNfaImcfAMC6TVlD5rl8+bISiYQOHTqk3t5eXblyxfYhAQAArLlNmyF7+umndfHiRV28eFFf+tKXdP36dTU1NenSpUu6fPnygg1kAY/XysJrhOplxSTpM3/415KkkY+YzFRpsOKRptwEVHGN29piZ1KS9FDFqD/mP1W9L0naX2oWDt9TYsbEioLWFjuLzXedCr/p64y/b9T9OZk2rSg+mN0mSbrnXkrSLyf3SJJujpmGsDeS5jJ5L/i7Lxoy9x12S9eqB8z9FjU8dt/fA1kxANicNmWG7LnnnlNXV5e6u7v1pS99SZJZ1zKRSGhwcFAHDhywfIQAAABrZ1NmyOrr6xesF4tEIurt7dXTTz9t6ciQL7KXCJKk5ie+4f+c3m0yS0Vu0mro12aDgRWmZmxH1KTNHqo0mbGPbLvrD3nIPZsyVmzOttzjLhZeGUr7Y0plHmPCXQ5pPBM8xl23ZmwwXSlJuu3Wjt1M7fTHvDdpTub4cNKMGbpjUnelHwa1aO7JnSo3JWzqufIXAgDkp00ZkC12FuW3v/3tDToSAACA9bcppywBAAAKyabMkAGrdfjINyUF03iT24M/9bE9pph+vNZMMWbCwVRjOGIawO6pMm0uHqky84H7y+75Y6qKzJjdxWZMuTtVWRYK+WNSMlOUY2mz7146mGocnDWF+XdnzbTkT8fN1PzgVKU/JuGYIv737kYlSUXjZpqz6lbwHCvvmvtOlwSPK9HaAgDyERkyAAAAy8iQYUvx2jz0zCnqL78XtJ1wDphGqqHdJtNVUx20q/hYjWn6uiNsCvX3lZlmrwfKPvTH7HCL+GNupmyhbzVeZuxu2nRvn0wHb7WB6R2SpHenTBbsV6OmmP/t2w/7Y6YnTEat5H33ctRkwbysmCSVjpmf5xbzkxUDgPxDhgwAAMAyMmTYUrx2F16m7M4X9kmSnCeCGq7Znabpa2W5udxVFTR9rSwx2z5e6TZ/dZdF8urGpCAzVhoyyytNy2SvUkHySsl0uXtpFhK/MxPz93mZsbccsxzSe6OmliydDmrBQsPmrVlxx2yrGDSP9cPvfsUfk10rBgDIb2TIAAAALCMgAwAAsIwpS+Q9r8WFJFXcMtOP7/6Bmar0uthnioPxlRFTxL8vNixJ+lgk6MK/w13YcnuJuZ+Iuz5lVWjKHzPrTlFOZ8zlpHvnk5ng7TSYNq0t7kzHJEm/mtzt73tn3BTxj06bkws+GHQX0Pww7I8JD5nvSjW/DE5GAIDVCoW+NW9bJvOMhSPBXGTIAAAALCNDhrznZcUkKfmYaaRa8YEpgr/3CbM9HZ32x+yuNuO3lZqsV6Rk0t/nNYD11qksD90/Q+Vlxhy3tcW99DZ/351pcxz9KdPiwsuKSdLPPnxIkjQ2bm5Xcd0U/mcdhna/+C9mW/yApPlrc0q0twCArYQMGQAAgGVkyJB35i6LNLEvyEzNXRap+GFTA7atPKgBe6jSZMj2ViQlSTtLR/x9XmasMmRaW5SHTGZtKrsIzW1v4bW0cNzL29M1/pB3JnZJkt6dMNsGnJi/b3TUzYj90ly6JWna/vPgGMl+AUBhIUMGAABgGRky5J2qt03T1ic+2S5JGv6tWLAzd51tlZSYRb4jFUGBVrTUZM12lzmSgqyYJFUXm31p9468LFhl1lmW3rakt0j4jDlLcmimyh/zjrsc0p1Rs+/ecJDFmx0yZ1d6b75w0lxefe2r/hiv6SuZMmDtLXSmIWAbGTIAAADLCMgAAAAsY8oSeevup2KSJLezhCRpeptpd1FeZwr1H4qYy49EBv0xD4XNVOVDJd6U5dh9H2PW/c6S3dJicMb8POKuVzm3gF8KpiqTo5WSpMz75f6+ikEzHRq5YY71R985M+9xmaoEgMJChgwAAMAyMmTIG1941CzvcecLZlmk0f1m+9SuoHlr9CGTESsqMtmn/VVmeaSq4pQ/ZnuJyYhNu60sZrO+l0y7yx95bWRn3eWRvAJ+SXp/JiJJ+sWYafD6K8cU8A+NVfpjxidM4b4GzAkA5cngbIPqm/fPjAEAChMZMgAAAMvIkGFT8rJh2e791l5JUumYyTAVT5msU2gmyD6VFJuurY/EzBJIJUVu24usdYmibpuL4lB63mNMpkslBVmz5KzJen0wHfHH3EmZn98di0mSnElTHzbaH4zJFJtjrPrQHFv5veAxYj8bnv+EAQAFjQwZAACAZWTIYN3nGr/u//z6ta9JkmYSNyVJmc82+Pumt5ls0/BHTPZpdqdp1lpVM+GPiZabn0vc7NeOMpMNy14eyWv66i2GdG8mOIPSqyvzzqAcnjEZsvdT1f6Y/lFzNuVIyh2TdM+kLMkEx/FvWUstSfrxf2/zf/aavgIA4CFDBgAAYBkBGQAAgGVMWcK60Bu9/s+//qcXJEnhP/ivkqSxh4PvDGP7zZTgTMy0uSitNM0pqsuDlhblJWZfbeWQuZ/QtObyCvUri8yUZ8ot5JekSbftxY3xXe4+c/0XyV3+mOFxM1U5/oFphVF2130bBTOWKnGXx8yeqvTQ9BUAMBcZMgAAAMvIkGHDeUX8xaMms5WdMfL2je0Nu5dB2skrmi/dZjJb0WpTwF9VltX0NWxSU0UyRf2VxWbseLrMH1MaMq0whmdN09bRmWBZo/enTOuK98bN5eCEyYJNp4PvLhMDpsC/4q7ZVu6uyuS145Ckf33ZNH312ne8+s63FvpVAIB1oVDu51MmM7/tENYfGTIAAADLyJBhw3jtHl53M2Je9qjhzy74Y4o+7matas317FYSxQ+bjNi+7UlJ0nTatJbYWT7uj6lyM2JeFmx81mTGSt0GsZI0njHZt9FZczmTDlpUjM2Y8fcmTZ3Z0Jg5nqnxoM6seNK0zSgzh6E9r96673MmMwYAWAoyZAAAAJYRkAEAAFjGlCU2zNx2D+kaUzCfXQxffs9MLY7EzZ9m9nKTRe46lePTZlqxNpI095MJ1rIscm/gddwvCrn3nXU/Q273/aEpc3lnPOjCPzTh7rtjthWNm/spSwbfXfa+aaZFr772VbPhRXNBB34AwEqRIQMAALCMDBk23G//nil0n/1PprXE2J4gwzX4uMlIZYpNZiuzO6vpa5nbCDY8KUkqcjux7q1I+mMm3CJ+79IznQm+ewymzNqV70+Yy4mpoGDfW5cylHbXu5wwtysZC+6reDI4QUCSnvhkuzmehscWfsIArJnb0gHYrMiQAQAAWEaGDBvGq7GadZdF8ridKgw3WTYbNUsghctngnFFJiPm1Yx59WF+nZikMT9DFmS9JOm22+hVkkZSphFsasZk44bvBjVkZe+Z220bMNenTBJNb38zWAJpbq3Yaz/uEAAAq0FABgB5wHGcnOvhcFjhcNjS0QBYSCqVUioVlNrMfd8+CAEZ1oXX9HUmcdPf5tVYTVWbmfLJ7Wb75O4gw+UtHB5yG8KGS4MMWUWpqSErKXLPtpwx2aw7k0H2K+3Wik3Omj9tZ8r8h+WkguWRRtzFwafGTDat8j+CbNpUcFdm391MzvORWBwcdtTW1uZcb29v17PPPmvnYAAsqLOzUx0dK5s1ISADgDwwMDCgSCT4xkB2DNh8zp49qzNnzvjXHceZ92XqfgjIACAPRCKRnIAMwOazmlICAjKsi4XWcPxc49clSW5fVk3udIv0S4Mpy1DKTDmWRcy6lZGKSX+ftx7l3GL+yawC/rsTptns1Iz50x5Mmqr89EzQWiM9ZYr5S+6ZMaVZLS22/5t5jIpLP5IkpX73U/d9Pl67C4r6AQCrRdsLAAAAy8iQYU15LSFmPv8bknKbqDqPmGL6aZPEUsmEyVqldk/7Y4rLzPjSUnOZvSySt2TSpFvMP1ESFPx7Mu74wWHzIJn3zGOGioMsXLF7ueNtc5nO2lc6ah53KYX7ZMYAbEULNdPNZJ5ZYCTWEhkyAAAAy8iQYU15maXmJ74hSUp+NGg3kS5zM2I73GWR3NYWGisOxrgtLbxlkpyJ4PZePZlXOzY0ZorRxieCZZKKS8ztM+9VmA3uV47Yz4PvHlNuH9hUzFy+9XzQ9BUAABvIkAEAAFhGhgwrlt0sNbsBrCSVxw9Ikm4d3udvK3KXSAq5ZWUZ968vFAlqyLZVmyzY1LTZmd0YdtxdBHxs3JxSXOJmw2bvBacYZ9yzNL3HKL9rsnLeEkiS9Pb/bTJi3lmfAADYRoYMAADAMjJkq3T+/HmdOHFCdXV1chxH3d3disViOnz4sO1DA4CCs9AZgkA+CGUymcziw3A/oVAo5/rp06d14cKFVd2n4ziKRqMaHh7Om87c3vTf69e+Jkn61Im/kSSN785qW7E3908tXeXOK04FidqiGrMoa8jdNJu1r7TCTF+m0+Y+09PuvqGgqD80a/Zt/6l7m3HzmJFfBAu8zm4L5xyrdwKCJF197auLPFNgcWv5Hs7HzwObCMjWB20vVmY5718yZKt04sQJNTc3S5KamppUV1dn+YgAAEC+ISBbpUceeUTHjh2zfRhWZBf1q3a7pGA5oXR9VJI0sSfIihW5tftpd6WjYse0u5iJBYX76Rm3Bca4e1kZ7JueNH+uoWFzB0WTJhtW+V6QhZvYbS5H3bg4csNcLtTE1WtiW9bw2AOeJQAA64+ifgAAAMvIkK3S0NCQzp8/L0m6ceOGGhoadPLkSctHtb68zNJEy6f9bRM7TEYrXWoass64/VxLshbuzrhJr+zFxCUplH3drQFTuVtfNhb8iRZNm31l98z3iEjCbRD78eDm239uto3vMmNrfjp83+exlOWRAADYCARkq5RIJHKK+FtaWiRpTYIyx3FyrofDYYXD4fuMBmBLKpVSKpXyr8997wLAYgo+IHMcR319fUseP7edxaVLl3Kut7a26vjx42sSkNXW1uZcb29v17PPPrvq+wWwtjo7O9XRwWLzAFau4AOy7u7uZbWpuHjx4gPPpIzFYkomk+rp6Vl1L7KBgYGc02Q3S3asxO3CXzo662+rSoxIku78VkySNO0e9nR1MB1ZOmKmEUPp3PsLDQd/hplys7P0Q1O4P1sVDC5xzFRl+J65PrbH3N+2d4P7itwwnf7L75nbp3t/toxnBqzM2bNndebMGf+64zjzvlABwIMUfEB25MgRHTlyZEW3bWlpUWtr64pvv5hIJELfISAPUE4AYLUKPiBbjYWmOhOJhGKxmOrr6zf8eNaCV7C/lIL3sYdL/Z+dAzFJ0sRD5nrpqLkMz4Y0V7rMzZp5FyVBFi3yc/MnOev2es2MBicCV9x1LweDzJwkVVz6kf/zzOd/Q5JUnJqThgMAYBOj7cUqtLW1qampKWfbpUuX1NbWRmYLAAAsGRmyVThx4oSef/55xeNxxeNxXbhwQQ0NDTp37pztQ1uxpWTGRhr2SJJCWYmq6W3m0mv+6slkJ8jc8L/EzXql3cxYZiYYNOXFsW7SLPrO/JW9qnvvSJImP7Zr3jF7GT6vzu1VWlsAwKrNXZKKpZTWHgHZKkQiEZ07d079/f1KJBK6ePEimTEAALBsBGRroK6urqDWsJyqNhmuiR1BZmtvd1KSdPNITFLQBDaT9ReWcTNk3lmWJRPz68u8fZUmCabwcJCGG9tj7mwmcdPc3r3MRrNXoHCwkDi2EmrIAAAALCMgAwAAsIwpSzxQ8xPf8H8unjTTh1XlZj5ypK7M3+c1hK26Za5PmHp7lQ8G91U6Ygr0hz9qpiq9NhbT1cGY6DtmzrJ4yluTstjft/t1c+cU6gMAthoyZAAAAJaRIUOOLzxqTmX2C+ez9nlNV72GsGVZ6yePP2wuw0Pmcu+bE5KkxO+W+2NKxk1mrPI9c337v09Jkqa3BVkwbwmmoqExSVKp29pCkl59hwJeAMDWRIYMAADAMjJkkBQ0VC1qeEyS9P13TJ3W5xq/7o8p+3BckjS+OypJKh8MmrbufvGHkoIs2rtNFZKkXT8JljCarjSXNS+ZscPHPiNJ+teXg0WZDx/5piSp58cdOccFAMBWRoYMAADAMjJkkBQ0VP3t38ut05rcGZxJOflRUw9WedecbZm9gPfgn/ymJGnXvyYlSeEhc7vxh4KYf1fvRM591/x0WFJQtyZJPXPqxGj0CgCbz0JNeVlOaXXIkAEAAFhGQAYAAGAZU5YF7NNP/Y3/s7dmZNWtUUlBMX35Zxv8MW9+72s5t8+e3tz+MzMdefdTMUlSNDEjSbr38eBPLPRGb87tX3ML9z/5xxdW/iQAANgCCMgAAHmBxcSxlRGQFbDIP/5w3rbX3CJ6L0P2+rUgK/apEyajVpwy7S5++L2ggPMzf/jXkoJM2djesCTp7W+2+WOar5tGsFdf+2rOY/74v7cJAIBCRg0ZAACAZWTICtBCzVa99hJeI9hit0Gsl/mSpH/97lckBW0qsu8nFj8gSRp7/CFJ0rb/8b/M/d3Oaiw7mlqbJwAAwBZDhgwAAMAyAjIAAADLmLJEjuwifim3NcYTn2yXJM3WbpckjX36YX+fN0XpddpvfuIbknIL+LM78gMAgAABGQAAWDWWU1odAjJICgr0h499RpIUfcW0xCht+bQ/Jt37M0nS6+4JAIePfNPfN3fNyeLJ2XmP8eo79BACAGAh1JABAABYRoasgHg1YHOzWdm8TJk3JruGrOh3P5UztufKX9z3frxatOzWGA96XAAAChkBGQBg02GZJBQaArIC4i3m/SBeFsvLpkXcurHsfctBVgwAgMVRQwYAAGAZGTIAALAu5k490wbj/gjItqgHrVc5t3B/IQtNb3q3K3HXraSNBQAAa4MpSwAAAMvIkG0R3rJE6ZoqSbnZLy+ztZTM2IMUNTwmSXp1CScHAACApSMgAwBYRYsLgIBsy1mo9svLiH2u8etrft8AAGD1CMgAAMCGYAHy+yMg2yJmEjcXHeMtZ7QQrwaNMycBANh4nGUJAABgGRkyAMCGoogfmI+ALA9lN331CvaX08rCK+7PnsJkqhIAAHsIyAAAgDUsr2QQkOWhlTZ29TyouB8AAGw8AjIAwLqhXgxYGs6yhFWpVErPPvusUqmU7UPZcDz3wnzuW10o9K2cfwubkfQ/3ctCxu9hod/B3L+hQgnqCchgVSqVUkdHR0H+x8xzL8znDsn85/t9FXYgIvF7kJb6OyiEAI0pSwDAimzV/xgBGwjIAABLQgCGzWIrLsFEQLYJZTIZSZLjOJaPZP15z7EQnutcPPet+9y95+W9l1fDu49bt27l/L7C4bDC4fCK7jMafXHVx7U6k3MuCxW/h7X8HYRCf7Xs2wwPn1z142ZLpVI5pRgjIyOSlvZZEMqsxScG1tS7776r2tpa24cBYJUGBga0f//+Vd0HnwdA/lvKZwEB2SaUTqd1+/ZtVVdXKxQK2T4cAMuUyWQ0MjKivXv3qqhodedOpdNpJRIJlZaW5nwerCZDBmB9zM2QZTIZTU9PKx6PL/pZQEAGAABgGW0vAAAALCMgAwAAsIyADAAAwDICMgAAAMsIyAAAACwjIAMAALCMgAwAAMAyAjIAAADLCMgAAAAsIyADAACwjIAMG66/v18vvviizp8/r9bWVl2/fn1Jtzt//rz6+/slSY7j6MqVK+rp6VnPQ8Uy8dpiJXj9Cw+v+QIywAZ66623MpcvX865HovFMhcvXlz0tpJy/p0+fXo9DxXLxGuLleL1Lzy85vOxuDg21Pnz53Xu3LmcbW1tbXr55Zc1NDT0wNu2traqublZktTU1KS6urp1O04sH68tVorXv/Dwms9XYvsAUFja29vV29urS5cu+dsaGhr0/PPPq6enR4cPH77vbR955BEdO3ZsIw4TK8Bri5Xi9S88vObzEZBhQ50+fVoNDQ0525LJpCQpFott/AFhzfDaAsDKMWUJ61pbW9Xd3a0bN248cFxbW5tqamokSTdu3FBDQ4NOnjy5EYeIFeK1xVLw+hceXvP5yJDBqv7+fr388su6du3aomMTiYQuXLjgX29paZGkgn8Tb1a8tlgqXv/Cw2s+HxkyLJvjOOrr61vy+AfVDjU3N6u1tXVFtQRXrlzR8ePHFy0Yhx28toVjLT8TJF7/QsRrToYMK9Dd3Z3zzWYxFy9eXPAMmra2thX/hy2ZuqRkMrlowTg2Hq9tYVmrzwQPr3/h4TUnIMMKHDlyREeOHFnVfbzyyitqaGjw/8N2HEeSFIlEFhzf0tKi1tbWVT8u1h+vbeFZzWcCr3/h4TVfGJ36seG87u3Z2ZOuri7/jLyFLDQdkkgkFIvFVF9fv8ZHiJXitcVy8foXHl7zhRGQYUP19/frpZdeUjweV09Pj//v0qVL/hTGlStXFAqFdOXKFf92bW1tampqyrmvS5cuqa2t7b6ZF2wsXlusBK9/4eE1XxhTlthQTz75pPr6+vT888/nbF/sW9GJEyf0/PPPKx6PKx6P68KFC2poaJjXGR728NpiJXj9Cw+v+cI4yxJ5pb+/X4lEQvX19QX9TWor4rUtbLz+hYfXPBcBGQAAgGXUkAEAAFhGQAYAAGAZARkAAIBlBGQAAACWEZABAABYRkAGAABgGQEZAACAZQRkAAAAlhGQAQAAWEZABgAAYBkBGQAAgGUEZAAAAJYRkAEAAFhGQAYAAGAZARkAAIBlBGQAAACWEZABAABYRkAGbAKO4+j8+fM6f/687UMBYMn58+dVU1OjUCikRx55RFeuXLF9SNhABGSARa+88oqam5t14MABtbe32z4cAJacP39ely5d0sWLF3Xt2jXV19frySef1CuvvGL70LBBQplMJmP7IIBC1d/fr0QiocOHDysUCqmjo0Pnzp2zfVgANpDjODp06JDeeustRSIRf3tzc7P6+vo0NDRk8eiwUciQARbV1dXp8OHDtg8DgGVHjx7NCcYkqaWlRclkUj09PZaOChuJgAxYB1euXFmwHux+2wFsTUv5LIhEIrpw4cK8MbFYbL0PD5sIARmwDurr63XhwgW1trb623p6evTkk08qHo/bOzAAG2o1nwVXr15VLBYji14gSmwfALAV1dXV6fLly2psbFRzc7OOHj2qJ598UqdPn9axY8dsHx6ADbLSz4Lr16/r5Zdf1gsvvLCBRwubKOoH1tErr7yiU6dOKR6PKxaL6erVq/cdS1E/sHUt57PAcRw1Njaqvr5eL7300gYeJWxiyhJYR8eOHVN9fb0SiYQuX75s+3AAWLLUzwKCscJFQAaso+vXr6u7u1vxeFynTp2yfTgALFnKZwHBWGEjIAPWiffh+sILL+jy5cvq6uqiySNQgJb6WXD8+HGCsQJGUT+wTrxvuidPnpQkXbx40T+zirOmgMKxlM+C1tZWJRIJvfDCC/P6jtXX18/rUYath6J+YB20traqq6tLN2/ezPkgbWtr08svv6xr167p4MGD6unpUWNj47zbNzU1PbDoF0B+WOpnQSgUuu99XLt2jS9xBYCADAAAwDJqyAAAACwjIAMAALCMgAwAAMAyAjIAAADLCMgAAAAsIyADAACwjIAMAADAMgIyAAAAywjIAAAALCMgAwAAsIyADAAAwDICMgAAAMsIyAAAACz7/wGPZqZ211mIHwAAAABJRU5ErkJggg==",
      "text/plain": [
       "<Figure size 650x650 with 3 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plotChain(chain_full, param_names)\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
