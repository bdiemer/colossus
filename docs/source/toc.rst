======================
Table of contents
======================

.. toctree::
    :maxdepth: 2
    
    installation
    versions
    tutorials
    faq
    modules
    utils
    global
